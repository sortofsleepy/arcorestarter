package com.example.arstarter

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

class CameraPermissionHelper {
    var CAMERA_PERMISSION_CODE:Number = 0;
    var CAMERA_PERMISSION:String = Manifest.permission.CAMERA;


    companion object {
        // Check to see we have the necessary permissions for this app.
        fun hasCameraPermission(activity:Activity): Boolean {
            return ContextCompat.checkSelfPermission(activity,Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
        }
        // Check to see we have the necessary permissions for this app and request if we don't
        fun requestCameraPermission(activity: Activity){
            var permissions = Array<String>(2){
                Manifest.permission.CAMERA.toString()
            }
            ActivityCompat.requestPermissions(activity,permissions,0)
        }


    }
}