package com.example.arstarter

import android.opengl.GLES32
import android.opengl.GLSurfaceView
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10


/**
 * Viewer for Ar stuff. Handles OpenGL related issues as well.
 */
class ARView : GLSurfaceView.Renderer{

    private var surfaceView:GLSurfaceView? = null

    constructor(){
        surfaceView?.preserveEGLContextOnPause = true;
        surfaceView?.setEGLContextClientVersion(2);
        surfaceView?.setEGLConfigChooser(8, 8, 8, 8, 16, 0); // Alpha used for plane blending.
        surfaceView?.setRenderer(this);
        surfaceView?.renderMode = GLSurfaceView.RENDERMODE_CONTINUOUSLY;
        surfaceView?.setWillNotDraw(false);
    }


    override fun onDrawFrame(gl: GL10?) {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT or GLES32.GL_DEPTH_BUFFER_BIT)
    }

    override fun onSurfaceChanged(gl: GL10?, width: Int, height: Int) {
        TODO("Not yet implemented")
    }

    override fun onSurfaceCreated(gl: GL10?, config: EGLConfig?) {
        GLES32.glClearColor(0.0f,0.0f,0.0f,1.0f)



    }
}